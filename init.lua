minetest.register_node("colornode:red", {
    description = "red",
    tiles = {
        "red.png"
    },
groups = {oddly_breakable_by_hand = 3}
})

minetest.register_node("colornode:orange", {
    description = "orange",
    tiles = {
        "orange.png"
    },
groups = {oddly_breakable_by_hand = 3}
})

minetest.register_node("colornode:yellow", {
    description = "yellow",
    tiles = {
        "yellow.png"
    },
groups = {oddly_breakable_by_hand = 3}
})

minetest.register_node("colornode:green", {
    description = "green",
    tiles = {
        "green.png"
    },
groups = {oddly_breakable_by_hand = 3}
})

minetest.register_node("colornode:blue", {
    description = "blue",
    tiles = {
        "blue.png"
    },
groups = {oddly_breakable_by_hand = 3}
})

minetest.register_node("colornode:purple", {
    description = "purple",
    tiles = {
        "purple.png"
    },
groups = {oddly_breakable_by_hand = 3}
})